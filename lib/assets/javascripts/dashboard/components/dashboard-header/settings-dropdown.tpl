<ul class="SettingsDropdown">
  <li>
    <div class="SettingsDropdown-sameline">
      <p class="CDB-Text CDB-Size-medium"><%- name %></p>
      <p class="SettingsDropdown-accountType CDB-Text CDB-Size-small u-altTextColor u-upperCase"><%- accountType %></p>
    </div>
    <p class="CDB-Text CDB-Size-medium u-altTextColor u-tSpace u-ellipsis">
      <%- email %>
    </p>
  </li>
  <li class="u-tSpace-xl">
    <p class="SettingsDropdown-userRole">
      <% if (isViewer) { %>
        <span class="UserRoleIndicator Viewer CDB-Text CDB-Size-small is-semibold u-altTextColor">FREE</span>
        <% if (orgDisplayEmail) { %>
          <a href="mailto:<%- orgDisplayEmail %>" class="CDB-Text CDB-Size-small">Become a Builder</a>
        <% } %>
      <% } %>
      <% if (isBuilder) { %>
        <span class="UserRoleIndicator Builder CDB-Text CDB-Size-small is-semibold u-altTextColor">PREMIUM</span>
      <% } %>
    </p>
  </li>
  <li class="u-tSpace-xl">
    <% if (showUpgradeLink) { %>
      <a href="<%- upgradeUrl %>" class="SettingsDropdown-itemLink">
    <% } %>

    <div class="SettingsDropdown-sameline u-bSpace CDB-Text CDB-Size-medium u-altTextColor">
      <p class="DefaultDescription"><%- usedDataStr %> di <%- availableDataStr %> usati</p>
      <% if (showUpgradeLink) { %>
        <p class="SettingsDropdown-itemLinkText u-actionTextColor">Aggiorna</p>
      <% } %>
    </div>
    <div class="SettingsDropdown-progressBar <%- progressBarClass %>">
      <div class="progress-bar">
        <span class="bar-2" style="width: <%- usedDataPct %>%"></span>
      </div>
    </div>

    <% if (showUpgradeLink) { %>
      </a>
    <% } %>
  </li>
</ul>
<div class="BreadcrumbsDropdown-listItem is-dark CDB-Text CDB-Size-medium">
  <ul>
    <li class="u-bSpace--m"><a href="<%- publicProfileUrl %>">Guarda il tuo profilo pubblico</a></li>
    <li class="u-bSpace--m"><a href="<%- accountProfileUrl %>">Il tuo Account</a></li>
    <% if (isOrgAdmin) { %>
      <li class="u-bSpace--m"><a href="<%- organizationUrl %>">La tua Organizzazione</a></li>
    <% } %>
    <% if (engineEnabled || mobileAppsEnabled) { %>
      <li class="u-bSpace--m"><a href="<%- apiKeysUrl %>">La tua API key</a></li>
    <% } %>
    <li><a href="<%- logoutUrl %>">Esci</a></li>
  </ul>
</div>
