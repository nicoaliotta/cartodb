<div class="u-inner">
  <div class="SupportBanner-inner">
    <div class="SupportBanner-info">
      <h4 class="CDB-Text CDB-Size-large u-secondaryTextColor u-bSpace">
        <% if (userType === 'org_admin' || userType === 'client') { %>
          Come cliente pagante, hai accesso al nostro supporto dedicato.
        <% } else if (isViewer) { %>
            Contatta <a href="mailto:<%- orgDisplayEmail %>">il team di Cartonico</a> per diventare Cliente Premium.
        <% } else if (userType === 'org') { %>
          Contatta <a href="mailto:<%- orgDisplayEmail %>">il team di Cartonico</a> per supporto.
        <% } else if (userType === "internal") { %>
          Fai parte di CARTONICO, meriti un supporto adeguato.
        <% } else { %>
          Per tutte le domande tecniche, contatta il nostro forum di supporto della comunità.
        <% } %>
      </h4>
      <p class="CDB-Text CDB-Size-medium u-altTextColor">
        <% if (isViewer) { %>
          Sarai in grado di creare le tue mappe!
        <% } else if (userType === 'org' || userType === 'org_admin' || userType === 'client') { %>
          Ricorda che ci sono molte informazioni nel nostro <a href="http://gis.stackexchange.com/questions/tagged/carto" target="_blank">forum di supporto</a>.
        <% } else if (userType === "internal") { %>
          Non dimenticare di condividere le tue conoscenze nel nostro <a href="http://gis.stackexchange.com/questions/tagged/carto"  target="_blank">forum di supporto</a>.
            <% } else { %>
          Se riscontri problemi con il servizio CARTONICO, sentiti libero di <a href="mailto:support@carto.com">contattarci</a>.
        <% } %>
      </p>
    </div>
    <% if (userType === 'org_admin') { %>
      <a href="mailto:enterprise-support@carto.com" class="SupportBanner-link CDB-Button CDB-Button--secondary">
        <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-medium u-upperCase">Contatta Supporto</span>
      </a>
    <% } else if (userType === 'org') { %>
        <a href="mailto:<%- orgDisplayEmail %>" class="SupportBanner-link CDB-Button CDB-Button--secondary">
          <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-medium u-upperCase">Contatta amministratore</span>
        </a>
    <% } else if (userType === 'client' || userType === 'internal') { %>
      <a href="mailto:support@carto.com" class="SupportBanner-link CDB-Button CDB-Button--secondary">
        <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-medium u-upperCase">Contattaci</span>
      </a>
    <% } else { %>
      <a href="http://gis.stackexchange.com/questions/tagged/carto" class="SupportBanner-link CDB-Button CDB-Button--secondary" target="_blank">
        <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-medium u-upperCase">Comunità di Supporto</span>
      </a>
    <% } %>
  </div>
</div>
