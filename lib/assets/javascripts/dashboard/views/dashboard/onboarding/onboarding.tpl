<div class="OnBoarding-map js-onboarding-map CDB-Text"></div>
<div class='OnBoarding-welcome'>
  <div class="OnBoarding-welcomeContent OnBoarding-welcomeContent--wide">
    <div class="CDB-Text OnBoarding-welcomeContentInner">
      <h4 class="OnBoarding-welcomeContentTitle OnBoarding-welcomeContentMargin">Ciao, <%- username %>!</h4>
      <p class="OnBoarding-welcomeContentText OnBoarding-welcomeContentMargin">Benvenuto a CARTONICO!</p>
      <% if(hasCreateMapsFeature) { %>
        <p class="OnBoarding-welcomeContentText OnBoarding-welcomeContentMargin">Trascina il tuo file dati nella dashboard per connettere il dataset e creare la tua prima mappa.</p>
        <button class="CDB-Button CDB-Button--primary u-tSpace-xl js-createMap track-onboarding--newMap">
          <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-small u-upperCase">Nuova Mappa</span>
        </button>
      <% } %>
    </div>
    <div class="OnBoarding-importAnimation"></div>
  </div>
</div>
