
/**
 *  Map templates
 *
 *  It will display all the possibilities to select
 *  any of your current datasets or connect a new dataset.
 *
 */

module.exports = [
  {
    short_description: 'Unisci il tuo dataset attuale con un altro dataset esistente.',
    guide_url: 'https://carto.com/learn/guides/analysis/join-column-from-second-layer',
    icon: 'analysis'
  },
  {
    short_description: 'Stilizza il tuo layer in base ai valori delle colonne.',
    guide_url: 'https://carto.com/learn/guides/styling/style-by-value',
    icon: 'styling'
  }
];
