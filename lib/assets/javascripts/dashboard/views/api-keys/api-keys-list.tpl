<section>
  <header class="ApiKeys-title">
    <h3 class="CDB-Text CDB-Size-medium is-semibold u-mainTextColor">La tua Api key</h3>
    <button type="submit" class="CDB-Button CDB-Button--primary js-add">
      <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-small u-upperCase">Nuova API key</span>
    </button>
  </header>

  <ul class="ApiKeys-list js-api-keys-list"></ul>
</section>

<% if (showGoogleApiKeys) { %>
<section>
  <div class="FormAccount-title">
    <p class="FormAccount-titleText">Configura le API key da provider esterni</p>
  </div>

  <span class="FormAccount-separator"></span>

  <div class="FormAccount-row">
    <div class="FormAccount-rowLabel">
      <label class="CDB-Text CDB-Size-medium is-semibold u-mainTextColor FormAccount-label">Google Maps</label>
    </div>
    <div class="FormAccount-rowData">
      <input type="text" value="<%- googleApiKey %>" class="CDB-InputText CDB-Text FormAccount-input FormAccount-input--long is-disabled" readonly />
    </div>
    <div class="FormAccount-rowInfo">
      <% if (!isInsideOrg) { %>
        <p class="CDB-Text CDB-Size-small u-altTextColor">
          Questa è la tua Google Maps query string, contatta <a href="mailto:nicoaliotta@gmail.com">nicoaliotta@gmail.com</a> per cambiarla.
        </p>
      <% } else if (isOrgOwner) { %>
        <p class="CDB-Text CDB-Size-small u-altTextColor">
          This is the <%= organizationName %> Google Maps query string, contatta <a href="nicoaliotta@gmail.com">nicoaliotta@gmail.com</a> per cambiarla.
        </p>
      <% } else { %>
        <p class="CDB-Text CDB-Size-small u-altTextColor">Questa è la Google Maps API key</p>
      <% } %>
    </div>
  </div>
</section>
<% } %>

<footer class="ApiKeys-footer">
  <p class="ApiKeys-footer-text">
    <i class="CDB-IconFont CDB-IconFont-info ApiKeys-footer-icon"></i>
    <span>Ulteriori informazioni sull'autorizzazione delle app e sulla gestione delle chiavi API <a href="https://carto.com/developers/fundamentals/authorization/" target="_blank">qui</a></span>
  </p>
</footer>
