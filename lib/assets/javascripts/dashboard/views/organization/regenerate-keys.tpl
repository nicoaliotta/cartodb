<%
if (scope === 'organization') {
  person_scope = 'Tu e tutti gli user del tuo team';
} else if (scope === 'organization_user') {
  person_scope = 'Utente';
} else {
  person_scope = 'Tu';
}
%>

<div class="CDB-Text Dialog-header u-inner">
  <div class="Dialog-headerIcon Dialog-headerIcon--negative">
    <i class="CDB-IconFont CDB-IconFont-keys"></i>
  </div>
  <p class="Dialog-headerTitle u-ellipsLongText">
  <% if (scope === 'organization') { %>
    Stai per rigenerare le <%- type === "api" ? 'API keys' : 'OAuth credentials' %> per tutti gli utenti del team
  <% } else if (scope === 'organization_user') { %>
   Stai per rigenerare le <%- type === "api" ? 'API keys' : 'OAuth credentials' %> per questo utente del team
  <% } else { %>
   Stai per rigenerare le tue <%- type === "api" ? 'API keys' : 'OAuth credentials' %>
  <% } %>
  </p>
  <p class="Dialog-headerText">
    <% if (type === "api") { %>
      <%- person_scope %> dovrai aggiornare tutte le app sviluppate con la nuova API key. Sei sicuro di voler continuare?
    <% } else { %>
      <%- person_scope %>  dovrai aggiornare tutte le OAuth keys nelle app dove stai usando CARTONICO. Sei sicuro?
    <% } %>
  </p>
</div>
<form action="<%- form_action %>" method="post">
  <% if (passwordNeeded) { %>
    <div class="CDB-Text Dialog-body">
      <div class="Form-row Form-row--centered has-label">
        <div class="Form-rowLabel">
          <label class="Form-label">La tua password</label>
        </div>
        <div class="Form-rowData">
          <input type="password" id="deletion_password_confirmation" name="password_confirmation" class="CDB-InputText CDB-Text Form-input Form-input--long" value=""/>
        </div>
      </div>
    </div>
  <% } %>

  <div class="Dialog-footer u-inner">
    <button type="button" class="CDB-Button CDB-Button--secondary js-cancel">
      <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-small u-upperCase">annulla</span>
    </button>

    <input name="utf8" type="hidden" value="&#x2713;" />
    <input name="_method" type="hidden" value="<%- method %>" />
    <input name="authenticity_token" type="hidden" value="<%- authenticity_token %>" />
    <button type="submit" class="CDB-Button CDB-Button--error">
      <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-small u-upperCase">Regenera le <%- type === "api" ? 'API keys' : 'OAuth credentials' %></span>
    </button>
  </div>
</form>
